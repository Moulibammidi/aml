import numpy as np
import warnings

warnings.filterwarnings("ignore")


class Evaluate:

    def __init__(self, true_samples, true_labels, model, adv_samples):
        self.true_samples = true_samples
        self.true_labels = true_labels
        self.model = model
        self.adv_samples = adv_samples
        self.Targeted = False
        self.targets_samples = np.zeros(true_labels.shape, dtype=float)

        predictions = model.predict(adv_samples)

        def soft_max(x):
            return np.exp(x) / np.sum(np.exp(x), axis=0)

        tmp_soft_max = []
        for i in range(len(predictions)):
            tmp_soft_max.append(soft_max(predictions[i]))

        self.softmax_prediction = np.array(tmp_soft_max)

    # help function
    def successful(self, adv_softmax_preds, nature_true_preds, targeted_preds, target_flag):
        if target_flag:
            if np.argmax(adv_softmax_preds) == np.argmax(targeted_preds):
                return True
            else:
                return False
        else:
            if np.argmax(adv_softmax_preds) != np.argmax(nature_true_preds):
                return True
            else:
                return False

    # 1 MR:Misclassification Rate
    def misclassification_rate(self):
        cnt = 0
        for i in range(len(self.adv_samples)):
            if self.successful(adv_softmax_preds=self.softmax_prediction[i], nature_true_preds=self.true_labels[i],
                               targeted_preds=self.targets_samples[i], target_flag=self.Targeted):
                cnt += 1
        mr = cnt / len(self.adv_samples)
        print('Misclassification Rate : {:.1f}%'.format(mr * 100))
        # return mr

    # 2 ACAC: average confidence of adversarial class
    def avg_confidence_adv_class(self):
        cnt = 0
        conf = 0
        for i in range(len(self.adv_samples)):
            if self.successful(adv_softmax_preds=self.softmax_prediction[i], nature_true_preds=self.true_labels[i],
                               targeted_preds=self.targets_samples[i], target_flag=self.Targeted):
                cnt += 1
                conf += np.max(self.softmax_prediction[i])

        print(
            'Average Confidence Of Adversarial Class : {:.3f}'.format(conf / cnt))
        # return conf / cnt

    # 3 ACTC: Average Confidence Of True Class
    def avg_confidence_true_class(self):

        true_labels = self.true_labels
        cnt = 0
        true_conf = 0
        for i in range(len(self.adv_samples)):
            if self.successful(adv_softmax_preds=self.softmax_prediction[i], nature_true_preds=self.true_labels[i],
                               targeted_preds=self.targets_samples[i], target_flag=self.Targeted):
                cnt += 1
                true_conf += self.softmax_prediction[i, true_labels[i]]
        print('Average Confidence Of True Class : {:.3f}'.format(
            true_conf / cnt))

        # return true_conf / cnt

    # 4 ALP: Average L_p Distortion
    def avg_lp_distortion(self):

        ori_r = np.round(self.true_samples * 255)
        adv_r = np.round(self.adv_samples * 255)

        NUM_PIXEL = int(np.prod(self.true_samples.shape[1:]))

        pert = adv_r - ori_r

        dist_l0 = 0
        dist_l2 = 0
        dist_li = 0

        cnt = 0

        for i in range(len(self.adv_samples)):
            if self.successful(adv_softmax_preds=self.softmax_prediction[i], nature_true_preds=self.true_labels[i],
                               targeted_preds=self.targets_samples[i], target_flag=self.Targeted):
                cnt += 1
                dist_l0 += (np.linalg.norm(
                    np.reshape(pert[i], -1), ord=0) / NUM_PIXEL)
                dist_l2 += np.linalg.norm(np.reshape(
                    self.true_samples[i] - self.adv_samples[i], -1), ord=2)
                dist_li += np.linalg.norm(np.reshape(
                    self.true_samples[i] - self.adv_samples[i], -1), ord=np.inf)

        adv_l0 = dist_l0 / cnt
        adv_l2 = dist_l2 / cnt
        adv_li = dist_li / cnt

        print('Average L_p Distortion:\n\tL0:\t{:.3f}\n\tL2:\t{:.3f}\n\tLi:\t{:.3f}\n'.format(adv_l0, adv_l2, adv_li))
        # return adv_l0, adv_l2, adv_li

    # 5 PSD: Perturbation Sensitivity Distance
    def avg_PSD(self):

        psd = 0
        cnt = 0

        for outer in range(len(self.adv_samples)):
            if self.successful(adv_softmax_preds=self.softmax_prediction[outer],
                               nature_true_preds=self.true_labels[outer],
                               targeted_preds=self.targets_samples[outer], target_flag=self.Targeted):
                cnt += 1

                image = self.true_samples[outer]
                pert = abs(self.adv_samples[outer] - self.true_samples[outer])

                for idx_channel in range(image.shape[0]):
                    image_channel = image[idx_channel]
                    pert_channel = pert[idx_channel]

                    image_channel = np.pad(image_channel, 1, 'reflect')
                    pert_channel = np.pad(pert_channel, 1, 'reflect')

                    for i in range(1, image_channel.shape[0] - 1):
                        for j in range(1, image_channel.shape[1] - 1):
                            psd += pert_channel[i, j] * (1.0 - np.std(np.array(
                                [image_channel[i - 1, j - 1], image_channel[i - 1, j], image_channel[i - 1, j + 1],
                                 image_channel[i, j - 1],
                                 image_channel[i, j], image_channel[i, j +
                                                                    1], image_channel[i + 1, j - 1],
                                 image_channel[i + 1, j],
                                 image_channel[i + 1, j + 1]])))
        print('Perturbation Sensitivity Distance : {:.3f}'.format(psd / cnt))
        # return psd / cnt

    # 6 NTE: Noise Tolerance Estimation
    def avg_noise_tolerance_estimation(self):

        nte = 0
        cnt = 0
        for i in range(len(self.adv_samples)):
            if self.successful(adv_softmax_preds=self.softmax_prediction[i], nature_true_preds=self.true_labels[i],
                               targeted_preds=self.targets_samples[i], target_flag=self.Targeted):
                cnt += 1
                sort_preds = np.sort(self.softmax_prediction[i])
                nte += sort_preds[-1] - sort_preds[-2]

        print('Noise Tolerance Estimation : {:.3f}'.format(nte / cnt))
        # return nte / cnt
