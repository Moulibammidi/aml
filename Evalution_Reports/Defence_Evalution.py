import numpy as np
import warnings

warnings.filterwarnings("ignore")

class Evaluate:

    def __init__(self, true_samples, true_labels, model, adv_samples):
        self.true_samples = true_samples
        self.true_labels = true_labels
        self.model = model
        self.adv_samples = adv_samples

        predictions = model.predict(adv_samples)

        def soft_max(x):
            return np.exp(x) / np.sum(np.exp(x), axis=0)

        tmp_soft_max = []
        for i in range(len(predictions)):
            tmp_soft_max.append(soft_max(predictions[i]))

        self.softmax_prediction = np.array(tmp_soft_max)

    # help function
    def successful(self, adv_softmax_preds, nature_true_preds):
        if np.argmax(adv_softmax_preds) == np.argmax(nature_true_preds):
            return True
        else:
            return False

    def avg_confidence_adv_class(self):
        cnt = 0
        conf = 0
        for i in range(len(self.adv_samples)):
            if self.successful(adv_softmax_preds=self.softmax_prediction[i], nature_true_preds=self.true_labels[i]):
                cnt += 1
                conf += np.max(self.softmax_prediction[i])

        print(
            'Average Confidence Of Adversarial Class : {:.3f}'.format(1.0-(conf / cnt)))
        return 1.0-(conf / cnt)

    def avg_confidence_true_class(self):

        true_labels = self.true_labels
        cnt = 0
        true_conf = 0
        for i in range(len(self.adv_samples)):
            if self.successful(adv_softmax_preds=self.softmax_prediction[i], nature_true_preds=self.true_labels[i]):
                cnt += 1
                true_conf += self.softmax_prediction[i, true_labels[i]]
        print('Average Confidence Of True Class : {:.3f}'.format(true_conf / cnt))

        return true_conf / cnt
        
