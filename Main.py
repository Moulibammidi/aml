import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import tensorflow as tf
from Models import Model as M
from Attacks import Black_Box_Attack as B, White_Box_Attack as W
from Defences import Defence as D
from Evalution_Reports import Attack_Evalution as AE, Defence_Evalution as DE
import cv2
from tqdm import tqdm
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix, f1_score
import seaborn as sn


sn.set(font_scale=1.4)

tf.compat.v1.disable_eager_execution()

White_box = {"DF": "DeepFool", "PGD": "Project Gradient Descent",
                 "FGM": "Fast Gradient Method", "C&W": "CarliniL2Method"}
Black_box = {"HSJ": "HopSkipJump", "SBA": "SimBA", "BA": "Boundary"}
Defence = {"BCT": "Basic Class Trainer", "Advtr": "Adversarial Trainer",
           "AdvPGD": "Adversarial_TrainerMadry_PGD"}
def Read_from_stdin():
    data = []
    path = [input("Enter the paths to ur dataset\n")]
    cmd = ""
    while(cmd != "S"):
        print("Choose Attack from :\n".join("%s : %s.\n" % (x,y) for (x,y) in White_box.items())\
            .join("%s : %s.\n" % (x,y) for (x,y) in Black_box.items()))
        a=input()
        if(a in White_box.keys() or a in Black_box.keys()):
            data.append(a)
        else:
            raise Exception("Unknown Attack Method found...")
        print("Choose Defence from :\n".join("%s : %s.\n" % (x,y) for (x,y) in Defence.items()))
        a=input()
        if(a in Defence.keys()):
            data.append(a)
        else:
            raise Exception("Unknown Defence Method found...")
        cmd = input("Wanna add more Attacks and Defences Enter anything 'OR' To start  enter 'S':\n")
    return data, path


def Attack_Defence(process):
    print("Attack Method".center(100, "*"),"\n")
    i=0
    while(i < len(process)):
        # White Box Attacks
        if(process[i] in White_box.keys() and process[i+1] in Defence.keys()):
            attack = White_box[process[i]]
            defence = Defence[process[i+1]]

            adv_samples,att = WBA.Create_Attack(process[i])

            print(f" Performace of {attack} Attack ".center(len(attack)+31,"-"), "\n")
            EvalAttack(adv_samples)

            print(f" Model Perfomance with {attack} Attack ".center(len(attack) + 39, "-"), "\n")
            accuracy(adv_samples, Classifier)

            print("Defence Method".center(100,"*"),"\n")
            print(defence)
            trainer_Cls = DF.Create_Defence(process[i+1],att)

            print(f" Model Perfomance after {defence} Defence ".center(
                len(defence)+41,"+"), "\n")
            accuracy(adv_samples, trainer_Cls)

            print(f"Perfomance of {defence} Defence".center(len(defence)+42,"+"),"\n")
            EvalDefence(trainer_Cls,adv_samples) # Evalution Defence

        # Black Box Attacks
        elif (process[i] in Black_box.keys() and process[i+1] in Defence.keys()):
            attack = Black_box[process[i]]
            defence = Defence[process[i+1]]

            adv_samples,att = BBA.Create_Attack(process[i]) # Applying Attack

            print(f" Performace of {attack} Attack ".center(
                len(attack)+31, "-"), "\n")
            EvalAttack(adv_samples) # Evalution Attack

            print(f" Model Perfomance with {attack} Attack ".center(
                len(attack) + 39, "*"), "\n")
            accuracy(adv_samples, Classifier)

            trainer_Cls = DF.Create_Defence(process[i+1], att) # Applying Defence
            print(f" Model Perfomance after {defence} Defence ".center(
                len(defence)+41, "+"), "\n")
            accuracy(adv_samples,trainer_Cls) # Model Evalution

            print(f"Perfomance of {defence} Defence".center(len(defence)+42,"+"),"\n")
            EvalDefence(trainer_Cls,adv_samples) # Evalution Defence
        i+=2


def accuracy(x, Cls):
    # We take the highest probability of Vector of probabilities
    pred_labels = np.argmax(Cls.predict(x), axis=1)

    print("F1_score of Model : {}".format(f1_score(y_test, pred_labels)*100))


def EvalAttack(adv_samples):
    EV = AE.Evaluate(x_test, y_test, Classifier, adv_samples)
    EV.misclassification_rate()
    EV.avg_confidence_adv_class()
    EV.avg_confidence_true_class()
    EV.avg_PSD()
    EV.avg_lp_distortion()
    EV.avg_noise_tolerance_estimation()


def EvalDefence(classifier, adv_samples):
    DV = DE.Evaluate(x_test, y_test, classifier, adv_samples)
    return DV.avg_confidence_adv_class(), DV.avg_confidence_true_class()


def load_data(path):
    class_names = os.listdir(path[0])
    class_names_label = {class_name: i for i,
                         class_name in enumerate(class_names)}
    nb_classes = len(class_names)
    IMAGE_SIZE = (180, 180)

    datasets=path
    output = []

    # Iterate through training and test sets
    for dataset in datasets:

        images = []
        labels = []

        print("Loading {}".format(dataset))

        # Iterate through each folder corresponding to a category
        for folder in os.listdir(dataset):
            label = class_names_label[folder]

            # Iterate through each image in our folder
            for file in tqdm(os.listdir(os.path.join(dataset, folder))):
                # Get the path name of the image
                img_path = os.path.join(os.path.join(dataset, folder), file)

                # Open and resize the img
                image = cv2.imread(img_path)
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                image = cv2.resize(image, IMAGE_SIZE)

                # Append the image and its corresponding label to the output
                images.append(image)
                labels.append(label)

        images = np.array(images, dtype='float32')
        labels = np.array(labels, dtype='int32')

        output.append((images, labels))
    if(len(path)==1):
        return images,labels
    else:
        return output


if(input("Wanna Give User Inputs Enter 'Y' OR press anykey to run with Default Inputs\n") != 'Y'):
    path = ['.../Dataset/train', '.../Dataset/test']
    (x_train, y_train), (x_test, y_test) = load_data(path)
    process=["DF","Advtr"]
else:
    process, path = Read_from_stdin()
    # without test and train folders
    if(len(path) == 1):
        data, labels = load_data(path)
        x_train, x_test, y_train, y_test = train_test_split(
            data,labels, test_size=0.2, random_state=42)

    # with test and train folders clearly
    else:
        (x_train, y_train), (x_test, y_test) = load_data(path)

x_train, y_train = shuffle(x_train, y_train, random_state=42)
x_train = x_train / 255.0
x_test = x_test / 255.0

# call the model
Classifier = M.Model(x_train, y_train).Create_Model()

print("Model Perfomance Before Attack".center(38, "*"),"\n")
accuracy(x_test, Classifier)

# create objects for the Classes
WBA = W.White_Box_Attack(Classifier, x_test)
BBA = B.Black_Box_Attack(Classifier, x_test, y_test)
DF = D.Defence(Classifier, x_train, y_train)

# call the function to apply attacks and defences 
Attack_Defence(process)
