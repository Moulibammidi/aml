# Project Title

Deep learning (DL) models are inherently vulnerable to adversarial examples – maliciously crafted inputs to trigger target DL models to misbehave – which significantly hinders the application of DL in security-sensitive domains. Intensive research on adversarial learning has led to an arms race between
adversaries and defenders.

Such plethora of emerging attacks and defenses raise many questions:

   * Which attacks are more evasive, preprocessing-proof, or transferable? 
   
   * Which defenses are more effective, utility-preserving, or general?
  
   * Are ensembles of multiple defenses more robust than individuals? 

Yet, due to the lack of platforms for comprehensive evaluation on adversarial attacks and defenses, these critical questions remain largely unsolved.

This Project will enables re-searchers and practitioners to

* measure the vulnerability of DL models.

* evaluate the effectiveness of various attacks/defenses.

* conduct comparative studies on attacks/defenses in a comprehensive and informative manner.

## Getting Started

Install Git using below command.

```bash
https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
```

Go to the folder where u want to clone repository and Right click there open the git command prompt.

Clone the repo using below [git](https://git-scm.com/doc) command.

```bash
git clone https://bitbucket.org/Moulibammidi/aml.git
```

### Prerequisites
* Minimium system requirements are : 8gb Ram,i5-5th generation,Basic intel GPU card.
* python3
* openCV
* numpy
* pandas
* Tensorflow
* Sklearn,Keras
* ART toolbox library


### Installing

Standard installation only....
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install ...

```bash
pip install Tensorflow
pip install adversarial-robustness-toolbox etc...
```

## Running the tests

Running the Main module using following command.It will ask for step-by-step process inputs.

```python
python3 Main.py
```

## Deployment

U can use Django to deploy the application to Web

## Built With

Project is totally build with python 3.8.0.

## Contributing

The project members are :

* Bammidi Mouli - moulibammidi70@gmail.com
* Hemanth Reddy - hemanthreddy433@gmail.com
* Pallavi Bhoi - pallavibhoi000@gmail.com
* abhaybidwai - abhaybidwai@gmail.com


## License

This project is licensed under the [CCL](www.ccl.org/) License.

## Acknowledgments

* https://github.com/kleincup/DEEPSEC
* https://github.com/P2333/Papers-of-Robust-ML
* https://adversarial-robustness-toolbox.readthedocs.io/en/stable/
* https://openai.com/blog/adversarial-example-research/
* https://www.pyimagesearch.com/2020/10/19/adversarial-images-and-attacks-with-kerasand-tensorflow/
* https://www.ibm.com/blogs/research/2020/10/adversarial-robustness-toolbox-one-yearlater-with-v1-4/
* https://github.com/cleverhans-lab/cleverhans
* https://www.pyimagesearch.com/2020/10/19/adversarial-images-and-attacks-with-keras-andtensorflow/
* https://youtu.be/b89RRzWSJYI
* https://advertorch.readthedocs.io/en/latest/advertorch/attacks.html
* https://advertorch.readthedocs.io/en/latest/advertorch/defenses.html
* https://adversarial-ml-tutorial.org/introduction/
* https://github.com/Trusted-AI/adversarial-robustness-toolbox
* etc
