import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import numpy as np

from art.attacks.evasion import DeepFool, ProjectedGradientDescent, FastGradientMethod, CarliniL2Method


class White_Box_Attack():
    def __init__(self, Classifier, x_test):
        self.Classifier = Classifier
        self.x_tst = x_test

    def Create_Attack(self, att):

        if(att == "DF"):

            print("Applying DeepFool Attack".center(32, ">"),"\n")

            attack = DeepFool(classifier=self.Classifier, max_iter=5,
                              epsilon=0.2, nb_grads=10, batch_size=5)

        elif(att == "PGD"):  # Projected Gradient Descent Attack

            print("Applying Projected Gradient Descent Attack".center(50, ">"),"\n")

            attack = ProjectedGradientDescent(estimator=self.Classifier, norm=np.inf,
                                              eps=0.2, eps_step=0.01, max_iter=20,
                                              targeted=False, num_random_init=0,
                                              batch_size=5, random_eps=False, verbose=False)

        elif(att == "FGM"):  # FastGradientMethod

            print("Applying FastGradientMethod Attack".center(42, ">"),"\n")
            attack = FastGradientMethod(estimator=self.Classifier, norm=np.inf, eps=0.2, eps_step=0.01,
                                        targeted=False, num_random_init=0, batch_size=5, minimal=False)

        elif(att == "CW"):  # CarliniL2Method

            print("Applying CarliniL2Method Attack".center(39, ">"),"\n")
            attack = CarliniL2Method(classifier=self.Classifier, confidence=0.0, targeted=False,
                                     learning_rate=0.01, binary_search_steps=10, max_iter=10,
                                     initial_const=0.01, max_halving=5, max_doubling=5,
                                     batch_size=5, verbose=True)

        test_adv = attack.generate(self.x_tst)

        return test_adv, attack
